package com.cpt304.factory.controllers;

import com.cpt304.factory.products.Burger;
import com.cpt304.factory.products.BeefBurger;
import com.cpt304.factory.products.VeggieBurger;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/restaurant")
public class Restaurant {
    @GetMapping("/burger/{request}")
    public Burger orderBurger(@PathVariable("request") String request){
        if("BEEF".equals(request)){
            BeefBurger burger = new BeefBurger();
            burger.prepare();
            return burger;
        }else{
            VeggieBurger burger = new VeggieBurger();
            burger.prepare();
            return burger;
        }
    }
}
