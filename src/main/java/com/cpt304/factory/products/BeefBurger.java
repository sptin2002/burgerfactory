package com.cpt304.factory.products;

public class BeefBurger implements Burger{
    private String name;

    public BeefBurger(){
        this.name = "Beef Burger";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void prepare(){
        System.out.println("Preparing Beef Burger");
    }
}
