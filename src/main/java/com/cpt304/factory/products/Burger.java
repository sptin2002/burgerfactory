package com.cpt304.factory.products;

public interface Burger {
    public void prepare();
}
