package com.cpt304.factory.products;

public class VeggieBurger implements Burger{
    private String name;

    public VeggieBurger(){
        this.name = "Veggie Burger";
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void prepare(){
        System.out.println("Preparing Veggie Burger");
    }
}
